<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 */
class EventsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Only allow users with the 'admin' role access to this controller
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user with a role of 'admin',
     * otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        if ($user['role'] === 'admin') {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $events = $this->Events->find('all')
            ->order(['Events.crawled_at' => 'DESC'])
            ->toArray();

        $this->set(compact('events'));
        $this->set('_serialize', ['events']);
    }

    /**
     * Approve an event to be in the live site results.
     *
     * @param int $id The event's id.
     *
     * @return void
     */
    public function approve($id)
    {
        $this->request->allowMethod(['post']);
        $event = $this->Events->find()
            ->where(['Events.id' => $id])
            ->first();
        $event->approved = 1;
        $event->touched = 1;

        if ($this->Events->save($event)) {
            $this->Flash->success(__('{0} has been approved.', $event->title));
        } else {
            $this->Flash->error(__('{0} could not be approved. Please, try again.', $event->title));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Deny an event to be in the live site results.
     *
     * @param int $id The event's id.
     *
     * @return void
     */
    public function deny($id)
    {
        $this->request->allowMethod(['post']);
        $event = $this->Events->find()
            ->where(['Events.id' => $id])
            ->first();
        $event->approved = 0;
        $event->touched = 1;

        if ($this->Events->save($event)) {
            $this->Flash->success(__('{0} has been denied.', $event->title));
        } else {
            $this->Flash->error(__('{0} could not be denied. Please, try again.', $event->title));
        }
        return $this->redirect(['action' => 'index']);
    }
}
