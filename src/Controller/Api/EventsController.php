<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 */
class EventsController extends AppController
{

    /**
     * Allow all authenticated users to view the Event index action
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user, otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        if ($user) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * API endpoint to return a JSON response of events for the main event
     * listing page. Contains the user entity of the currently authenticated
     * user, if the user is tracking the event - in order to keep track of their
     * event tracking status.
     *
     * @return void
     */
    public function index()
    {
        $userId = $this->Auth->user('id');
        $events = $this->Events->find('all')
            ->where(['Events.approved' => true])
            ->contain([
                'Users' => function ($q) use ($userId) {
                    return $q->where(['Users.id' => $userId]);
                }
            ])
            ->order(['Events.event_date' => 'ASC'])
            ->toArray();

        $this->set(compact('events'));
        $this->set('_serialize', ['events']);
    }
}
