<div class="container">
  <div class="row">
    <div class="col-xs-3">
      <h2>New User</h2>
      <?= $this->Form->create($user); ?>
       <div class="form-group"> 
        <?=
        $this->Form->input('email', [
          'class' => 'form-control',
          'placeholder' => 'Email address'
        ])
        ?>
      </div>
      <div class="form-group"> 
        <?=
          $this->Form->input('password', [
            'class' => 'form-control',
            'placeholder' => 'Password'
          ])
        ?>
      </div>
      <div class="form-group"> 
        <?=
          $this->Form->input('role', [
            'type' => 'select',
            'class' => 'form-control',
            'options' => [
              'admin' => 'Admin',
              'normal' => 'Normal'
            ]
          ])
        ?>
      </div>
      <div class="form-group"> 
        <?=
        $this->Form->button(__('Create'), [
          'class' => 'btn btn-success'
        ])
        ?>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
