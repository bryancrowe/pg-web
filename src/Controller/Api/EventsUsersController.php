<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * EventsUsers Controller
 *
 * @property \App\Model\Table\EventsUsersTable $EventsUsers
 */
class EventsUsersController extends AppController
{
 
    /**
     * Allow authenticated users to add/delete their tracked events.
     * They're bound by their session user id, so allow everything to go through
     * regardless of any passed params.
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user, otherwise default to false
     */   
    public function isAuthorized($user = null)
    {
        if ($user) {
            return true;
        }
        return parent::isAuthorized($user);
    }

    /**
     * API endpoint for adding a tracked event.
     *
     * @param int $eventId The event's id
     *
     * @return void
     */
    public function add($eventId = null)
    {
        $this->request->allowMethod(['post']);
        $eventsUser = $this->EventsUsers->newEntity();
        $eventsUser->event_id = $eventId;
        $eventsUser->user_id = $this->Auth->user('id');

        if ($this->EventsUsers->save($eventsUser)) {
            $this->set(compact('eventsUser'));
            $this->set('_serialize', 'eventsUser');
        }
    }

    /**
     * API endpoint for removing a tracked event.
     *
     * @param int $eventId The event's id
     *
     * @return void
     */
    public function delete($eventId = null)
    {
        $this->request->allowMethod(['delete']);
        $deleted = $this->EventsUsers->deleteAll([
            'event_id' => $eventId,
            'user_id' => $this->Auth->user('id')
        ]);

        if ($deleted) {
            $this->set(compact('data'));
            $this->set('_serialize', 'data');
        }
    }
}
