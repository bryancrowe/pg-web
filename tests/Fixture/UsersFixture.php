<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'email' => ['type' => 'string', 'length' => 2048, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'password' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'role' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 2,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 3,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 4,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 5,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 6,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 7,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 8,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 9,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
        [
            'id' => 10,
            'email' => 'Lorem ipsum dolor sit amet',
            'password' => 'Lorem ipsum dolor sit amet',
            'role' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:46:00',
            'modified' => '2015-07-03 21:46:00'
        ],
    ];
}
