<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EventsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EventsController Test Case
 */
class EventsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events',
        'app.users',
        'app.events_users',
        'app.referrals'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/events');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'login']);
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->get('/events');
        $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/events/this-is-a-special-event');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'login']);

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->get('/events/this-is-a-special-event');
        $this->assertResponseOk();
    }
}
