<?php
echo $this->Html->script('/components/angular/angular.min', ['block' => 'script']);
echo $this->Html->script('events', ['block' => 'script']);
?>
<div id="events" class="container container-top-padding" ng-app="eventsApp" ng-controller="EventCtrl">
  <div class="row">
    <div class="col-xs-12">
      <div class="row" ng-show="events.length < 1">
        <div class="col-xs-12 text-center">
          <h3>No results found.</h3>
        </div>
      </div>
      <div class="row event" ng-repeat="event in events">
        <div class="col-xs-6 col-xs-offset-3">
          <div class="row event-details">
            <div class="col-xs-9">
              <a href="/events/{{ event.slug }}">{{ event.title }}</a>&nbsp;&nbsp;
              <a href="/referrals/refer/{{event.id}}" target="blank"><i class="fa fa-external-link"></i></a>
              <p>{{ event.event_date | date:'MMMM d' }} at {{ event.event_date | date:'h:mma' }}</p>
              <p class="seen">
                <strong>Seen:</strong> {{ event.crawled_at | date:'MMMM d y'}}
                on {{ sourceNames[event.source ] }}
              </p>
            </div>
            <div class="col-xs-3 text-right">
              <a class="btn btn-default btn-xs" ng-click="trackEvent(event.id, 'POST')" ng-show="event.users.length < 1">
                <i class="fa fa-heart-o"></i>&nbsp;&nbsp;TRACK EVENT
              </a>
              <a class="btn btn-success btn-xs" ng-click="trackEvent(event.id, 'DELETE')" ng-show="event.users.length > 0">
                <i class="fa fa-heart"></i>&nbsp;&nbsp;TRACKING
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
