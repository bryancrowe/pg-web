<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Admin\UsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\UsersController Test Case
 */
class AdminUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.events',
        'app.events_users',
        'app.referrals'
    ];

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);
        $this->get('/admin/users/add');
        $this->assertResponseOk();
    }
}
