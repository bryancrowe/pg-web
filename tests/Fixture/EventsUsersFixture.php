<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EventsUsersFixture
 *
 */
class EventsUsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'event_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'user_idx' => ['type' => 'index', 'columns' => ['user_id', 'event_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['event_id', 'user_id'], 'length' => []],
            'events_users_ibfk_1' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'events_users_ibfk_2' => ['type' => 'foreign', 'columns' => ['event_id'], 'references' => ['events', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'event_id' => 1,
            'user_id' => 1
        ],
        [
            'event_id' => 2,
            'user_id' => 2
        ],
        [
            'event_id' => 3,
            'user_id' => 3
        ],
        [
            'event_id' => 4,
            'user_id' => 4
        ],
        [
            'event_id' => 5,
            'user_id' => 5
        ],
        [
            'event_id' => 6,
            'user_id' => 6
        ],
        [
            'event_id' => 7,
            'user_id' => 7
        ],
        [
            'event_id' => 8,
            'user_id' => 8
        ],
        [
            'event_id' => 9,
            'user_id' => 9
        ],
        [
            'event_id' => 10,
            'user_id' => 10
        ],
    ];
}
