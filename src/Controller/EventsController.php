<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 */
class EventsController extends AppController
{

    /**
     * Call AppController's beforeEvent() method
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Allow auth'd users to view the events list and detail pages
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user, otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        if ($user) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('events', $this->paginate($this->Events));
        $this->set('_serialize', ['events']);
    }

    /**
     * view method
     *
     * Shows the detailed view of an event.
     *
     * @param string $slug The event's URL slug
     *
     * @return void
     */
    public function view($slug = null)
    {
        $event = $this->Events->find()
            ->where(['slug' => $slug])
            ->first();

        $this->set(compact('event'));
    }
}
