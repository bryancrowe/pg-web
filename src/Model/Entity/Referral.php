<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Referral Entity.
 */
class Referral extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'event_id' => true,
        'user_id' => true,
        'user_agent' => true,
        'ip_address' => true,
        'referer' => true,
        'event' => true,
        'user' => true,
    ];
}
