<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReferralsFixture
 *
 */
class ReferralsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'event_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_agent' => ['type' => 'string', 'length' => 2048, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'ip_address' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'referer' => ['type' => 'string', 'length' => 2048, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'event_id' => 1,
            'user_id' => 1,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 2,
            'event_id' => 2,
            'user_id' => 2,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 3,
            'event_id' => 3,
            'user_id' => 3,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 4,
            'event_id' => 4,
            'user_id' => 4,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 5,
            'event_id' => 5,
            'user_id' => 5,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 6,
            'event_id' => 6,
            'user_id' => 6,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 7,
            'event_id' => 7,
            'user_id' => 7,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 8,
            'event_id' => 8,
            'user_id' => 8,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 9,
            'event_id' => 9,
            'user_id' => 9,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
        [
            'id' => 10,
            'event_id' => 10,
            'user_id' => 10,
            'user_agent' => 'Lorem ipsum dolor sit amet',
            'ip_address' => 'Lorem ipsum dolor sit amet',
            'referer' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 21:45:55'
        ],
    ];
}
