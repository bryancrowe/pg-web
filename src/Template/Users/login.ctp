<div id="login" class="container container-top-padding">
  <div class="row">
    <div class="col-xs-4 col-xs-offset-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Login</h3>
        </div>
        <div class="panel-body">
          <?= $this->Form->create() ?>
          <div class="form-group">
            <?=
            $this->Form->input('email', [
              'class' => 'form-control',
              'placeholder' => 'Email address'
            ])
            ?>
            <?=
            $this->Form->input('password', [
              'class' => 'form-control',
              'placeholder' => 'Password'
            ])
            ?>
          </div>
          <div class="form-group">
            <?=
            $this->Form->button(__('LOG IN'), [
              'class' => 'btn btn-primary btn-block'
            ])
            ?>
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
