<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EventsUser Entity.
 */
class EventsUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'event' => true,
        'user' => true,
    ];
}
