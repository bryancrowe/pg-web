<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>My Events</h2>
    </div>
    <div class="col-xs-12">
      <table class="table">
        <thead>
          <tr>
            <th>Event</th>
            <th>Date &amp; Time</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($eventsUsers as $eventsUser): ?>
          <tr>
            <td><?= $this->Html->link($eventsUser->event->title, "/events/{$eventsUser->event->slug}") ?></td>
            <td><?= $eventsUser->event->event_date ?></td>
            <td class="text-right">
              <?=
                $this->Form->postLink(
                  __('UNTRACK'),
                  [
                    'action' => 'delete',
                    $eventsUser->event_id
                  ],
                  [
                    'class' => 'btn btn-xs btn-danger',
                    'target' => '_blank',
                    'confirm' => __('Are you sure you want to untrack {0}?', $eventsUser->event->title)
                  ]
                )
              ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
