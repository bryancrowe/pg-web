<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Admin\EventsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\EventsController Test Case
 */
class AdminEventsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events',
        'app.users',
        'app.events_users',
        'app.referrals'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/admin/events');
        $this->assertRedirect('/users/login');
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->get('/events');
        $this->assertResponseOk();
    }

    /**
     * Test approve method
     *
     * @return void
     */
    public function testApprove()
    {
        $this->post('/admin/events/approve/1');
        $this->assertRedirect('/users/login');
    
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->post('/admin/events/approve/1');
        $this->assertRedirect('/admin/events');
    }

    /**
     * Test deny method
     *
     * @return void
     */
    public function testDeny()
    {
        $this->post('/admin/events/deny/1');
        $this->assertRedirect('/users/login');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->post('/admin/events/deny/1');
        $this->assertRedirect('/admin/events');
    }
}
