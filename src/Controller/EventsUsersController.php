<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * EventsUsers Controller
 *
 * @property \App\Model\Table\EventsUsersTable $EventsUsers
 */
class EventsUsersController extends AppController
{

    /**
     * Call AppController's beforeEvent() method
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Allow auth'd users to view their "My Events" list, and untrack events
     * they've subscribed to. We don't have to worry about the passed params
     * since deletes are bound to session user ids.
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user, otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        if ($user) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'finder' => [
                'byUser' => [
                    'user' => $this->Auth->user('id')
                ]
            ],
            'contain' => ['Events', 'Users']
        ];
        $this->set('eventsUsers', $this->paginate($this->EventsUsers));
        $this->set('_serialize', ['eventsUsers']);
    }

    /**
     * Delete method
     *
     * @param string|null $eventId Events id.
     *
     * @return void Redirects to index.
     */
    public function delete($eventId = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $eventsUser = $this->EventsUsers->find()
            ->where([
                'event_id' => $eventId,
                'user_id' => $this->Auth->user('id')
            ])
            ->contain(['Events'])
            ->first();

        if ($this->EventsUsers->delete($eventsUser)) {
            $this->Flash->success(__('{0} has been untracked.', $eventsUser->event->title));
        } else {
            $this->Flash->error(__('{0} could not be untracked. Please, try again.', $eventsUser->event->title));
        }
        return $this->redirect(['action' => 'index']);
    }
}
