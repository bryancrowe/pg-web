<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Api\EventsUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EventsUsersController Test Case
 */
class ApiEventsUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events_users',
        'app.events',
        'app.users',
        'app.referrals'
    ];

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->post('/api/events/1/track.json');
        $this->assertRedirect('/users/login');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);
        $result = $this->post('/api/events/1/track.json');

        $expected = [
            'event_id' => '1',
            'user_id' => 1
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->delete('/api/events/1/track.json');
        $this->assertRedirect('/users/login');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);
        
        $this->delete('/api/events/1/track.json');
        $this->assertResponseOk();
    }
}
