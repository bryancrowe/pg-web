<?php
echo $this->Html->script('https://maps.googleapis.com/maps/api/js?key=AIzaSyChznoRBb2RmPOWx67UqlGArukZsxe55I8', ['block' => 'script']);
$sourceNames = [
  'eventbrite' => 'EventBrite',
  'joonbug' => 'JoonBug'
];
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-6">
      <h2><?= $event->title ?> <a href="/referrals/refer/<?= $event->id ?>" target="blank"><i class="fa fa-external-link"></i></a></h2>
      <h3><?= $event->event_date->format('M j') ?> at <?= $event->event_date->format('g:iA') ?></h3>
      <p class="seen"><strong>Seen:</strong> <?= $event->crawled_at->format('M j, Y') ?> on <?= $sourceNames[$event->source] ?></p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
    <div class="col-xs-6 no-padding">
      <div id="map-canvas"></div>
    </div>
  </div>
</div>
<script>
var map;
function initialize() {

  // Yeah yeah, outputting PHP within JS is stupid, just not trying to rabbithole
  // for the sake of getting this example code done
  var latitude = <?= $event->latitude ?>;
  var longitude = <?= $event->longitude ?>;

  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(latitude, longitude)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  var marker = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(latitude, longitude)
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
