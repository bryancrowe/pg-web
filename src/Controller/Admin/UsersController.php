<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Only allow users with the 'admin' role access to this controller
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user with a role of 'admin',
     * otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        if ($user['role'] === 'admin') {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $events = $this->Users->Events->find('list', ['limit' => 200]);
        $this->set(compact('user', 'events'));
        $this->set('_serialize', ['user']);
    }
}
