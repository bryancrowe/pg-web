# PartyGeek Web App

Web application for SeatGeek.

## Requirements

* PHP 5.4+
* [Composer](https://getcomposer.org/)
* [Bower](http://bower.io/)
* [LESS](http://lesscss.org/)
* [PHPUnit](https://phpunit.de/)

## Installation

* Clone this repository.
* After installing Composer, run `composer install` in the root directory.
* After installing Bower, run `bower install` in your application's directory.
  (Note: this isn't nessecary as of now since I've included the Bower assets in
  the git repo)
* Drop in a working `config/app.php` file. To do this, give the
  `Datasources.default` key a valid database connection config. Also set the
  `Session.defaults` key's value to `database`.
* Run migrations to get the database schema. To do this, run
  `bin/cake migrations migrate -t 20150703212400` in the root directory.

## Running the Application

* From the root directory, run `bin/cake server`.
* View the application in your browser at `http://localhost:8765`.

## Running the Tests

* Duplicate the database for the test suite.
* Update the `Datasources.test` key in `config/app.php` to point to the test
* database.
* Run `phpunit` in the root directory.
