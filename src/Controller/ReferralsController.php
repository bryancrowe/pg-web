<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Referrals Controller
 *
 * @property \App\Model\Table\ReferralsTable $Referrals
 */
class ReferralsController extends AppController
{

    /**
     * Call AppController's beforeEvent() method
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Allow auth'd users to be referred, will eventually open up to unauth'd
     * users
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user, otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        if ($user) {
            return true;
        }
        return parent::isAuthorized($user);
    }

    /**
     * Proxy action to track referrals to other sites. Track the request's user
     * agent string, IP address, and the referring site. If there's an
     * authenticated user, track them as well. After that, save the referral
     * record and redirect the user to the source event listing.
     *
     * @param int $eventId The event's id.
     *
     * @return void
     */
    public function refer($eventId = null)
    {
        $event = $this->Referrals->Events->get($eventId);
        $referral = $this->Referrals->newEntity();
        $data = [
            'event_id' => $eventId,
            'user_agent' => $this->request->env('HTTP_USER_AGENT'),
            'ip_address' => $this->request->env('REMOTE_ADDR'),
            'referer' => $this->request->env('HTTP_REFERER')
        ];

        // In the future whenever the application isn't completely restricted
        // by authentication, we'll conditionally check for a User id.
        $userId = $this->Auth->user('id');
        if ($userId) {
            $data['user_id'] = $userId;
        }
        $referral = $this->Referrals->patchEntity($referral, $data);
        if ($this->Referrals->save($referral)) {
            $this->Flash->success(__('The referral has been saved.'));
            return $this->redirect($event->url);
        } else {
            $this->Flash->error(__('The referral could not be saved. Please, try again.'));
        }
    }
}
