<?php $user = $this->request->session()->read('Auth.User'); ?>
<nav id="top-nav" class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">PartyGeek</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <div class="nav navbar-nav navbar-right">
      <?php if (!$user): ?>
      <a class="btn btn-primary navbar-btn" href="/users/login">LOG IN</a></li>
      <?php else: ?>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <?= $user['email'] ?> <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/my-events">My Events</a></li>
            <li><a href="/users/logout">Log Out</a></li>
            <?php if ($user['role'] === 'admin'): ?>
            <li role="separator" class="divider"></li>
            <li class="dropdown-header">Admin</li>
            <li><a href="/admin/users/add">Create User</a></li>
            <li><a href="/admin/events">Events</a></li>
            <?php endif; ?>
          </ul>
        </li>
      </ul>
      <?php endif; ?>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="/events">EVENTS</a></li>
      </ul>
    </div>
  </div>
</nav>
