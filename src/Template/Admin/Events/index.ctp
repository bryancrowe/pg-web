<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>Site Events</h2>
    </div>
    <div class="col-xs-12">
      <table class="table">
        <thead>
          <tr>
            <th>Event</th>
            <th class="text-right">Crawled At</th>
            <th class="text-right">Referral Count</th>
            <th class="text-right">Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($events as $event): ?>
          <tr>
            <td>
              <a href="/events/<?= $event->slug ?>" target="_blank"><?= $event->title ?></a>
            </td>
            <td class="text-right">
              <?= $event->crawled_at ?>
            </td>
            <td class="text-right">
              <?= $event->referral_count ?>
            </td>
            <td class="text-right">
              <?php if (!$event->touched): ?>
                <i class="fa fa-square-o"></i>
              <?php else: ?>
                <?php if ($event->approved): ?>
                  <i class="fa fa-check-square-o"></i>
                <?php else: ?>
                  <i class="fa fa-minus-square-o"></i>
                <?php endif; ?>
              <?php endif; ?>
            </td>
            <td class="text-right">
              <?=
                $this->Form->postLink(
                  __('APPROVE'),
                  [
                    'action' => 'approve',
                    $event->id
                  ],
                  [
                    'class' => 'btn btn-xs btn-success',
                    'target' => '_blank',
                    'confirm' => __('Are you sure you want to approve {0}?', $event->title)
                  ]
                )
              ?>
              <?=
                $this->Form->postLink(
                  __('DENY'),
                  [
                    'action' => 'deny',
                    $event->id
                  ],
                  [
                    'class' => 'btn btn-xs btn-danger',
                    'target' => '_blank',
                    'confirm' => __('Are you sure you want to deny {0}?', $event->title)
                  ]
                )
              ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
