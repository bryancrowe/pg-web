var eventsApp = angular.module('eventsApp', [], function ($locationProvider) {});

eventsApp.controller('EventCtrl', function ($scope, $http, $location) {
  $scope.events = [];
  $scope.sourceNames = {
    eventbrite : 'EventBrite',
    joonbug: 'JoonBug'
  };

  $scope.getEvents = function () {
    var requestUrl = '/api/events.json';

    $http.get(requestUrl).success(function (data) {
      $scope.events = data.events;
    });
  };

  $scope.trackEvent = function (eventId, httpMethod) {
    var request = {
      url: '/api/events/' + eventId + '/track.json',
      method: httpMethod
    };

    $http(request).success(function (data) {
      $scope.getEvents();
    });
  };

  $scope.getEvents();
});
