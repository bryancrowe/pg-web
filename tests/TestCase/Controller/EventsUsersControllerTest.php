<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EventsUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EventsUsersController Test Case
 */
class EventsUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events_users',
        'app.events',
        'app.users',
        'app.referrals'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/my-events');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'login']);

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);
        $this->get('/my-events');
        $this->assertResponseOk();
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->get('/events-users/delete/1');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'login']);

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);
        $this->get('/events-users/delete/1');
        $this->assertResponseError();
    }
}
