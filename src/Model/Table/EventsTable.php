<?php
namespace App\Model\Table;

use App\Model\Entity\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Events Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class EventsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('events');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->belongsToMany('Users', [
            'foreignKey' => 'event_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'events_users'
        ]);
        $this->hasMany('Referrals', [
            'foreignKey' => 'event_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->allowEmpty('title');
            
        $validator
            ->allowEmpty('url');

        $validator
            ->allowEmpty('slug');
            
        $validator
            ->allowEmpty('event_date');
            
        $validator
            ->allowEmpty('location');
            
        $validator
            ->add('longitude', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('longitude');
            
        $validator
            ->add('latitude', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('latitude');

        $validator
            ->allowEmpty('source');

        $validator
            ->allowEmpty('crawled_at');

        $validator
            ->allowEmpty('checksum');

        $validator
            ->allowEmpty('referral_count');

        return $validator;
    }
}
