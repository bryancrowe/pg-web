<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Allow unauth'd users to access the login and logout actions
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'logout']);
    }

    /**
     * Return the parent isAuthorized() method by default
     *
     * @param array $user The authenticated user or null
     *
     * @return bool True if there is an auth'd user with a role of 'admin',
     * otherwise default to false
     */
    public function isAuthorized($user = null)
    {
        return parent::isAuthorized($user);
    }

    /**
     * Basic user authentication action
     *
     * @return void
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    /**
     * Basic user logout action
     *
     * @return void
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
