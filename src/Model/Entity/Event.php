<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity.
 */
class Event extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'url' => true,
        'slug' => true,
        'event_date' => true,
        'location' => true,
        'longitude' => true,
        'latitude' => true,
        'source' => true,
        'crawled_at' => true,
        'checksum' => true,
        'referral_count' => true,
        'approved' => true,
        'touched' => true,
        'users' => true,
    ];
}
