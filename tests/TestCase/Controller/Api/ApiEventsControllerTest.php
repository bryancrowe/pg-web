<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Api\EventsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ApiEventsController Test Case
 */
class ApiEventsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events',
        'app.users',
        'app.events_users',
        'app.referrals'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);
        $result = $this->get('/api/events.json');
        $this->assertResponseOk();

        // Expected output whenever Auth'd user isn't tracking the event
        $expected = [
            'events' => [
                [
                    'id' => 1,
                    'title' => 'Lorem ipsum dolor sit amet',
                    'url' => 'http://www.google.com',
                    'slug' => 'this-is-a-special-event',
                    'event_date' => '2015-07-03T21:45:30+0000',
                    'location' => 'Lorem ipsum dolor sit amet',
                    'longitude' => 1,
                    'latitude' => 1,
                    'source' => 'Lorem ipsum dolor sit amet',
                    'crawled_at' => '2015-07-03T21:45:30+0000',
                    'checksum' => 'Lorem ipsum dolor sit amet',
                    'referral_count' => 1,
                    'approved' => 1,
                    'touched' => 1,
                    'users' => []
                ]
            ]
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);

        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);
        $result = $this->get('/api/events.json');
        $this->assertResponseOk();

        // Expected output whenever Auth'd user *is* tracking the event
        $expected = [
            'events' => [
                [
                    'id' => 1,
                    'title' => 'Lorem ipsum dolor sit amet',
                    'url' => 'http://www.google.com',
                    'slug' => 'this-is-a-special-event',
                    'event_date' => '2015-07-03T21:45:30+0000',
                    'location' => 'Lorem ipsum dolor sit amet',
                    'longitude' => 1,
                    'latitude' => 1,
                    'source' => 'Lorem ipsum dolor sit amet',
                    'crawled_at' => '2015-07-03T21:45:30+0000',
                    'checksum' => 'Lorem ipsum dolor sit amet',
                    'referral_count' => 1,
                    'approved' => 1,
                    'touched' => 1,
                    'users' => [
                        [
                            'id' => 1,
                            'email' => 'Lorem ipsum dolor sit amet',
                            'password' => 'Lorem ipsum dolor sit amet',
                            'role' => 'Lorem ipsum dolor sit amet',
                            'created' => '2015-07-03T21:46:00+0000',
                            'modified' => '2015-07-03T21:46:00+0000',
                            '_joinData' => [
                                'user_id' => 1,
                                'event_id' => 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }
}
