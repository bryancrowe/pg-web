<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ReferralsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ReferralsController Test Case
 */
class ReferralsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.referrals',
        'app.events',
        'app.users',
        'app.events_users'
    ];

    /**
     * Test refer method
     *
     * @return void
     */
    public function testRefer()
    {
        $this->get('/referrals/refer/1');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'login']);

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'demo@example.com',
                    'role' => 'admin'
                ]
            ]
        ]);
        $this->get('/referrals/refer/1');
        $this->assertRedirect('http://www.google.com');
    }
}
